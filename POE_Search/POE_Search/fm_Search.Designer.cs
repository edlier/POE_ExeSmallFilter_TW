﻿namespace POE_Search
{
    partial class fm_Search
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_ItemName = new System.Windows.Forms.TextBox();
            this.btn_Search = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_MaxPrice = new System.Windows.Forms.TextBox();
            this.dgv_SearchResult = new System.Windows.Forms.DataGridView();
            this.pb_Item = new System.Windows.Forms.PictureBox();
            this.cb_leagues = new System.Windows.Forms.ComboBox();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.類型 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Corrupted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OnlineStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastAccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Whisper = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.picURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Whisper = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BaseValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.explicit_01 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.explicit_02 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.explicit_03 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.explicit_04 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.explicit_05 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.explicit_06 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SearchResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Item)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_ItemName
            // 
            this.tb_ItemName.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.tb_ItemName.Location = new System.Drawing.Point(145, 43);
            this.tb_ItemName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_ItemName.Name = "tb_ItemName";
            this.tb_ItemName.Size = new System.Drawing.Size(365, 39);
            this.tb_ItemName.TabIndex = 0;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(518, 42);
            this.btn_Search.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(127, 37);
            this.btn_Search.TabIndex = 1;
            this.btn_Search.Text = "Search";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 28);
            this.label1.TabIndex = 3;
            this.label1.Text = "Item Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 28);
            this.label2.TabIndex = 4;
            this.label2.Text = "Max:";
            // 
            // tb_MaxPrice
            // 
            this.tb_MaxPrice.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.tb_MaxPrice.Location = new System.Drawing.Point(145, 94);
            this.tb_MaxPrice.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_MaxPrice.Name = "tb_MaxPrice";
            this.tb_MaxPrice.Size = new System.Drawing.Size(90, 39);
            this.tb_MaxPrice.TabIndex = 5;
            // 
            // dgv_SearchResult
            // 
            this.dgv_SearchResult.AllowUserToAddRows = false;
            this.dgv_SearchResult.AllowUserToDeleteRows = false;
            this.dgv_SearchResult.AllowUserToOrderColumns = true;
            this.dgv_SearchResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv_SearchResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_SearchResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemName,
            this.類型,
            this.ItemLevel,
            this.Corrupted,
            this.Price,
            this.Currency,
            this.OnlineStatus,
            this.AccountId,
            this.LastAccountName,
            this.LastTime,
            this.Whisper,
            this.picURL,
            this.btn_Whisper,
            this.BaseValue,
            this.explicit_01,
            this.explicit_02,
            this.explicit_03,
            this.explicit_04,
            this.explicit_05,
            this.explicit_06});
            this.dgv_SearchResult.Location = new System.Drawing.Point(17, 157);
            this.dgv_SearchResult.Name = "dgv_SearchResult";
            this.dgv_SearchResult.ReadOnly = true;
            this.dgv_SearchResult.RowTemplate.Height = 31;
            this.dgv_SearchResult.Size = new System.Drawing.Size(1688, 488);
            this.dgv_SearchResult.TabIndex = 2;
            this.dgv_SearchResult.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SearchResult_CellClick);
            this.dgv_SearchResult.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_SearchResult_CellContentClick);
            this.dgv_SearchResult.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgv_SearchResult_CellPainting);
            this.dgv_SearchResult.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgv_SearchResult_RowPostPaint);
            // 
            // pb_Item
            // 
            this.pb_Item.Location = new System.Drawing.Point(672, 12);
            this.pb_Item.Name = "pb_Item";
            this.pb_Item.Size = new System.Drawing.Size(181, 139);
            this.pb_Item.TabIndex = 6;
            this.pb_Item.TabStop = false;
            // 
            // cb_leagues
            // 
            this.cb_leagues.FormattingEnabled = true;
            this.cb_leagues.Items.AddRange(new object[] {
            "掘獄聯盟",
            "標準模式"});
            this.cb_leagues.Location = new System.Drawing.Point(252, 94);
            this.cb_leagues.Name = "cb_leagues";
            this.cb_leagues.Size = new System.Drawing.Size(121, 36);
            this.cb_leagues.TabIndex = 7;
            // 
            // ItemName
            // 
            this.ItemName.DataPropertyName = "ItemName";
            this.ItemName.Frozen = true;
            this.ItemName.HeaderText = "物品名稱";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            this.ItemName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // 類型
            // 
            this.類型.DataPropertyName = "ItemType";
            this.類型.Frozen = true;
            this.類型.HeaderText = "類型";
            this.類型.Name = "類型";
            this.類型.ReadOnly = true;
            // 
            // ItemLevel
            // 
            this.ItemLevel.DataPropertyName = "ItemLevel";
            this.ItemLevel.HeaderText = "物等";
            this.ItemLevel.Name = "ItemLevel";
            this.ItemLevel.ReadOnly = true;
            this.ItemLevel.Width = 50;
            // 
            // Corrupted
            // 
            this.Corrupted.DataPropertyName = "Corrupted";
            this.Corrupted.HeaderText = "汙染";
            this.Corrupted.Name = "Corrupted";
            this.Corrupted.ReadOnly = true;
            this.Corrupted.Width = 50;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.HeaderText = "價錢";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 60;
            // 
            // Currency
            // 
            this.Currency.DataPropertyName = "Currency";
            this.Currency.HeaderText = "通貨";
            this.Currency.Name = "Currency";
            this.Currency.ReadOnly = true;
            this.Currency.Width = 80;
            // 
            // OnlineStatus
            // 
            this.OnlineStatus.DataPropertyName = "OnlineStatus";
            this.OnlineStatus.HeaderText = "狀態";
            this.OnlineStatus.Name = "OnlineStatus";
            this.OnlineStatus.ReadOnly = true;
            this.OnlineStatus.Width = 80;
            // 
            // AccountId
            // 
            this.AccountId.DataPropertyName = "AccountId";
            this.AccountId.HeaderText = "ID";
            this.AccountId.Name = "AccountId";
            this.AccountId.ReadOnly = true;
            // 
            // LastAccountName
            // 
            this.LastAccountName.DataPropertyName = "LastAccountName";
            this.LastAccountName.HeaderText = "帳號";
            this.LastAccountName.Name = "LastAccountName";
            this.LastAccountName.ReadOnly = true;
            // 
            // LastTime
            // 
            this.LastTime.DataPropertyName = "LastTime";
            this.LastTime.HeaderText = "最後更新";
            this.LastTime.Name = "LastTime";
            this.LastTime.ReadOnly = true;
            // 
            // Whisper
            // 
            this.Whisper.DataPropertyName = "Whisper";
            this.Whisper.HeaderText = "hide_密語";
            this.Whisper.Name = "Whisper";
            this.Whisper.ReadOnly = true;
            this.Whisper.Visible = false;
            // 
            // picURL
            // 
            this.picURL.DataPropertyName = "ImageURL";
            this.picURL.HeaderText = "PicUrl";
            this.picURL.Name = "picURL";
            this.picURL.ReadOnly = true;
            this.picURL.Visible = false;
            // 
            // btn_Whisper
            // 
            this.btn_Whisper.HeaderText = "私訊";
            this.btn_Whisper.Name = "btn_Whisper";
            this.btn_Whisper.ReadOnly = true;
            this.btn_Whisper.Text = "私訊";
            this.btn_Whisper.UseColumnTextForButtonValue = true;
            // 
            // BaseValue
            // 
            this.BaseValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.BaseValue.DataPropertyName = "BaseValue";
            this.BaseValue.HeaderText = "基底";
            this.BaseValue.Name = "BaseValue";
            this.BaseValue.ReadOnly = true;
            this.BaseValue.Width = 86;
            // 
            // explicit_01
            // 
            this.explicit_01.DataPropertyName = "explicit_01";
            this.explicit_01.HeaderText = "墜1";
            this.explicit_01.Name = "explicit_01";
            this.explicit_01.ReadOnly = true;
            // 
            // explicit_02
            // 
            this.explicit_02.DataPropertyName = "explicit_02";
            this.explicit_02.HeaderText = "墜2";
            this.explicit_02.Name = "explicit_02";
            this.explicit_02.ReadOnly = true;
            // 
            // explicit_03
            // 
            this.explicit_03.DataPropertyName = "explicit_03";
            this.explicit_03.HeaderText = "墜3";
            this.explicit_03.Name = "explicit_03";
            this.explicit_03.ReadOnly = true;
            // 
            // explicit_04
            // 
            this.explicit_04.DataPropertyName = "explicit_04";
            this.explicit_04.HeaderText = "墜4";
            this.explicit_04.Name = "explicit_04";
            this.explicit_04.ReadOnly = true;
            // 
            // explicit_05
            // 
            this.explicit_05.DataPropertyName = "explicit_05";
            this.explicit_05.HeaderText = "墜5";
            this.explicit_05.Name = "explicit_05";
            this.explicit_05.ReadOnly = true;
            // 
            // explicit_06
            // 
            this.explicit_06.DataPropertyName = "explicit_06";
            this.explicit_06.HeaderText = "墜6";
            this.explicit_06.Name = "explicit_06";
            this.explicit_06.ReadOnly = true;
            // 
            // fm_Search
            // 
            this.AcceptButton = this.btn_Search;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1730, 675);
            this.Controls.Add(this.cb_leagues);
            this.Controls.Add(this.pb_Item);
            this.Controls.Add(this.tb_MaxPrice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_SearchResult);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.tb_ItemName);
            this.Font = new System.Drawing.Font("微軟正黑體", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "fm_Search";
            this.Text = "Search";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fm_Search_FormClosing);
            this.Load += new System.EventHandler(this.fm_Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_SearchResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Item)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_ItemName;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_MaxPrice;
        private System.Windows.Forms.DataGridView dgv_SearchResult;
        private System.Windows.Forms.PictureBox pb_Item;
        private System.Windows.Forms.ComboBox cb_leagues;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn 類型;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Corrupted;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn OnlineStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountId;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastAccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Whisper;
        private System.Windows.Forms.DataGridViewTextBoxColumn picURL;
        private System.Windows.Forms.DataGridViewButtonColumn btn_Whisper;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn explicit_01;
        private System.Windows.Forms.DataGridViewTextBoxColumn explicit_02;
        private System.Windows.Forms.DataGridViewTextBoxColumn explicit_03;
        private System.Windows.Forms.DataGridViewTextBoxColumn explicit_04;
        private System.Windows.Forms.DataGridViewTextBoxColumn explicit_05;
        private System.Windows.Forms.DataGridViewTextBoxColumn explicit_06;
    }
}

