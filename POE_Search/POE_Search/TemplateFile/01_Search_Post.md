﻿-  POST Data to https://web.poe.garena.tw/api/trade/search/穿越聯盟

**POST Template :**

```
{
    "query": {
        "status": {
            "option": "online"
        },
        "name": "拉維安加之泉",
        "type": "聖化魔力藥劑",
        "stats": [
            {
                "type": "and",
                "filters": [],
                "disabled": false
            }
        ],
        "filters": {
            "trade_filters": {
                "filters": {
                    "sale_type": {
                        "option": "priced"
                    }
                },
                "disabled": false
            }
        }
    },
    "sort": {
        "price": "asc"
    }
}
```

**Result Back**
```
{
    "result":["efb11013720fd6ac87ea80517521b4af7e3ae056169731a8f02f230c0eabbe18",
    "342bc4e2948f5e51a34b14f4422da2d33e3ae7ec76b014f7583e8c2451d56033",
    "b9f5f52cc165fdd02de87a9d273282f37168a7f6d821b393dff05de25f38a1f8",
    "27dbc23fa889b0803f5a45e5dd9546fb949b510aea92663df3b58d2c4ba2cccd"],

    "id":"44mwgt9",
    "total":4
}
```