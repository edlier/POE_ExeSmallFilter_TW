﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Dynamic;
using System.Net;
using System.IO;
using System.Web;
using POE_Search.Data;
using System.Collections;

namespace POE_Search
{
    public partial class fm_Search : Form
    {
        private publicFuctions pf = new publicFuctions();
        string imageURL = "";

        public fm_Search()
        {
            InitializeComponent();
        }
        private void fm_Search_Load(object sender , EventArgs e)
        {
            dgv_SearchResult.AutoGenerateColumns = false;
            cb_leagues.SelectedIndex = 0;

        }
        private void btn_Search_Click(object sender , EventArgs e)
        {
            if (tb_ItemName.Text == String.Empty) return;

            // Split the word to get item name
            string[] searchName = (tb_ItemName.Text).Trim().Replace(" " , "").Replace("\t" , "").Split(',');

            string encodeLeages = HttpUtility.UrlEncode(cb_leagues.SelectedItem.ToString());
            string ApiURL_Post = "https://web.poe.garena.tw/api/trade/search/" + encodeLeages;
            // %E7%A9%BF%E8%B6%8A%E8%81%AF%E7%9B%9F -> Incursion league

            #region Combined Post Data
            dynamic dy = new JObject();

            dy.query = new JObject();
            dy.query.status = new JObject();
            //dy.query.stats = new JArray();
            dy.sort = new JObject();

            dy.query.filters = new JObject();
            dy.query.filters.trade_filters = new JObject();
            dy.query.filters.trade_filters.filters = new JObject();
            dy.query.filters.trade_filters.filters.sale_type = new JObject();


            dy.query.status.option = "online";

            if (searchName.Count() == 1)
            {
                dy.query.type = searchName[0];
            }
            else
            {
                dy.query.name = searchName[0];
                dy.query.type = searchName[1];
            }

            dynamic temp = new JObject();
            temp.type = "and";
            temp.filters = new JArray();
            dy.query.stats = new JArray(temp);

            dy.query.filters.trade_filters.disabled = false;
            dy.query.filters.trade_filters.filters.sale_type.option = "priced";
            if (tb_MaxPrice.Text != "")
            {
                dy.query.filters.trade_filters.filters.price = new JObject();
                dy.query.filters.trade_filters.filters.price.max = Convert.ToInt32(tb_MaxPrice.Text);
            }

            dy.sort.price = "asc";

            #endregion

            string sendData = JsonConvert.SerializeObject(dy).ToString();
            string postResult = PostAddHeader(ApiURL_Post , sendData);
            if (postResult == "") return;

            SetGetData(postResult);
        }

        void SetGetData(string postResult)
        {
            dynamic dy_PostResult = JsonConvert.DeserializeObject(postResult);
            string ApiURL_Get = "https://web.poe.garena.tw/api/trade/fetch/"; //+combined string

            int counter = 0;

            List<string> list_URL = new List<string>();
            foreach (var item in dy_PostResult.result)
            {
                ApiURL_Get += item + ",";
                counter++;
                if (counter == 10)
                {
                    counter = 0;
                    list_URL.Add(ApiURL_Get.Remove(ApiURL_Get.Count() - 1) + "?query=" + dy_PostResult.id);
                    ApiURL_Get = "https://web.poe.garena.tw/api/trade/fetch/";
                }
            }
            if (counter > 0) list_URL.Add(ApiURL_Get.Remove(ApiURL_Get.Count() - 1) + "?query=" + dy_PostResult.id);




            string result = "";
            foreach (string item in list_URL)
            {
                string getResult = GetHttpWebRequest(item);
                if (getResult == "") continue;
                string get = ReOrginizeGetData(getResult);
                result += get.Remove(get.Count() - 1).Remove(0 , 1) + ",";
            }
            result = result + "]";
            result = "[" + result.Substring(0 , result.Count());
            dynamic dy = JsonConvert.DeserializeObject(result);
            dgv_SearchResult.DataSource = dy;
            //if (imageURL != "")
            //{
            //    pb_Item.ImageLocation = imageURL;
            //    //pb_Item.Width =;
            //}
        }
        string ReOrginizeGetData(string getResult)
        {
            dynamic dy_newData = new JArray();
            dynamic dy_GetResult = JsonConvert.DeserializeObject(getResult);
            foreach (var item in dy_GetResult.result)
            {
                dynamic dy_Temp = new JObject();
                dy_Temp.ItemName = item.item.name.ToString();
                dy_Temp.ItemType = item.item.typeLine.ToString();
                dy_Temp.ItemLevel = item.item.ilvl;
                dy_Temp.Corrupted = item.item.corrupted == true ? "v" : "";
                //dy_Temp.Icon = "https://web.poe.garena.tw/" + item.item.icon.ToString();
                dy_Temp.ImageURL = "https://web.poe.garena.tw/" + item.item.icon.ToString();

                dy_Temp.LastTime = CalLastUpdateTime(item.listing.indexed.ToString());
                dy_Temp.AccountId = item.listing.account.name.ToString();
                dy_Temp.LastAccountName = item.listing.account.lastCharacterName.ToString();

                dy_Temp.OnlineStatus = "線上";
                if (item.listing.account.online == null) dy_Temp.OnlineStatus = "不在線";
                else
                {
                    try
                    {
                        if (item.listing.account.online.status == "afk") dy_Temp.OnlineStatus = "afk";
                    }
                    catch { }
                }

                dy_Temp.Price = item.listing.price.amount;
                dy_Temp.Currency = pf.FindDic_String_String(item.listing.price.currency.ToString() , Dic_Ob.dct_OB);

                dy_Temp.Whisper = item.listing.whisper.ToString();

                dy_Temp.BaseValue = item.item.implicitMods == null ? "" : item.item.implicitMods.ToString().Replace("[" , "").Replace("]" , "").Replace("\"" , "").Replace(" " , "");

                string[] explictValue = new string[10];
                explictValue = item.item.explicitMods == null ? null : SeperateExplicitValue(item.item.explicitMods.ToString());

                //bool haFixedValue = false;
                try
                {
                    //if (item.item.extended.mods != null)
                    //{
                    //    if (((Newtonsoft.Json.Linq.JContainer)item.item.extended.mods).Count == 2)
                    //    {
                    //        int ModCount = 0;
                    //        foreach(var ii in item.item.extended.mods)
                    //        {
                    //            if (ModCount == 1)
                    //            {
                    //                foreach (var iit in ii)
                    //                {
                    //                    for(int iitCount=0 ;iitCount< (iit).Count ; iitCount++)
                    //                    {
                    //                        if(iit[iitCount].magnitudes[0].min == iit[iitCount].magnitudes[0].max)
                    //                        {
                    //                            //固定數值
                    //                            dy_Temp.explicit_01 += dy_Temp.explicit_01 + explictValue[iitCount];
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //            ModCount++;
                    //        }
                    //    }
                    //}
                    dy_Temp.explicit_01 = explictValue[0] == null ? "" : explictValue[0];
                    dy_Temp.explicit_02 = explictValue[1] == null ? "" : explictValue[1];
                    dy_Temp.explicit_03 = explictValue[2] == null ? "" : explictValue[2];
                    dy_Temp.explicit_04 = explictValue[3] == null ? "" : explictValue[3];
                    dy_Temp.explicit_05 = explictValue[4] == null ? "" : explictValue[4];
                    dy_Temp.explicit_06 = explictValue[5] == null ? "" : explictValue[5];
                    dy_Temp.explicit_07 = explictValue[6] == null ? "" : explictValue[6];
                }
                catch { }

                dy_newData.Add(dy_Temp);
            }
            return dy_newData.ToString();
        }

        private string CalLastUpdateTime(string lastTime)
        {
            TimeSpan ts = DateTime.Now - Convert.ToDateTime(lastTime);
            if (ts.Days > 0) return (ts.Days.ToString()) + " 天前";
            else return (ts.Minutes.ToString()) + " 分鐘前";
        }
        private string[] SeperateExplicitValue(string exValue)
        {
            dynamic dy = JsonConvert.DeserializeObject(exValue);
            List<string> li = new List<string>();
            foreach (string item in dy)
            {
                li.Add(item);
            }
            return li.ToArray();
        }

        public string PostAddHeader(string postURL , string postData)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(postURL);
            request.Method = "POST";
            request.ContentType = "application/json";
            //request.Headers[headerName] = headerValue;
            //request.Timeout = 60 * 1000; //1mins

            using (StreamWriter reqStream = new StreamWriter(request.GetRequestStream()))
            {
                reqStream.Write(postData);
            }
            try
            {
                using (HttpWebResponse wr = (HttpWebResponse)request.GetResponse())
                {
                    string data;
                    using (var sr = new StreamReader(wr.GetResponseStream()))
                    {
                        data = sr.ReadToEnd();
                        wr.Close();
                        return data;
                    }
                }
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}" , httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                    }
                }
                return "";

            }
        }
        public string GetHttpWebRequest(string getURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(getURL);
            request.Method = "GET";
            //request.Timeout = 60 * 1000; //1mins
            //request.Headers[headerName] = headerValue;

            try
            {
                using (HttpWebResponse wr = (HttpWebResponse)request.GetResponse())
                {
                    string data;
                    using (var sr = new StreamReader(wr.GetResponseStream()))
                    {
                        data = sr.ReadToEnd();
                        wr.Close();
                        return data;
                    }
                }
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}" , httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {
                        string text = reader.ReadToEnd();
                        //Console.WriteLine(text);
                    }
                }
                return "";
            }
        }


        private void fm_Search_FormClosing(object sender , FormClosingEventArgs e)
        {
            Application.Exit();
        }
        private void dgv_SearchResult_RowPostPaint(object sender , DataGridViewRowPostPaintEventArgs e)
        {
            var grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center ,

                LineAlignment = StringAlignment.Center
            };
            //get the size of the string
            Size textSize = TextRenderer.MeasureText(rowIdx , this.Font);
            //if header width lower then string width then resize
            if (grid.RowHeadersWidth < textSize.Width + 40)
            {
                grid.RowHeadersWidth = textSize.Width + 40;
            }
            var headerBounds = new Rectangle(e.RowBounds.Left , e.RowBounds.Top , grid.RowHeadersWidth , e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx , this.Font , SystemBrushes.ControlText , headerBounds , centerFormat);
        }
        private void dgv_SearchResult_CellContentClick(object sender , DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgv_SearchResult.Columns["btn_Whisper"].Index && e.RowIndex!=-1)
            {
                string str_Whisper = dgv_SearchResult.Rows[e.RowIndex].Cells["Whisper"].Value.ToString();
                Clipboard.SetText(str_Whisper);
            }


        }
        private void dgv_SearchResult_CellClick(object sender , DataGridViewCellEventArgs e)
        {
            pb_Item.ImageLocation = null;
            try {
                string PicUrl = dgv_SearchResult.Rows[e.RowIndex].Cells["picURL"].Value.ToString();
                pb_Item.ImageLocation = PicUrl;
            }
            catch { }

        }

        private void dgv_SearchResult_CellPainting(object sender , DataGridViewCellPaintingEventArgs e)
        {
            if (this.dgv_SearchResult.Columns["OnlineStatus"].Index == e.ColumnIndex && e.RowIndex >= 0)
            {
                Brush color = Brushes.Green;
                string status = e.FormattedValue.ToString();
                if (status == "不在線")
                {
                    color = Brushes.Red;
                }
                else if (status == "afk")
                {
                    color = Brushes.Orange;
                }

                Rectangle newRect = new Rectangle(e.CellBounds.X + 1 ,
            e.CellBounds.Y + 1 , e.CellBounds.Width - 4 ,
            e.CellBounds.Height - 4);

                using (
                    Brush gridBrush = new SolidBrush(this.dgv_SearchResult.GridColor),
                    backColorBrush = new SolidBrush(e.CellStyle.BackColor))
                {
                    using (Pen gridLinePen = new Pen(gridBrush))
                    {
                        // Erase the cell.
                        e.Graphics.FillRectangle(backColorBrush , e.CellBounds);

                        // Draw the grid lines (only the right and bottom lines;
                        // DataGridView takes care of the others).
                        e.Graphics.DrawLine(gridLinePen , e.CellBounds.Left ,
                            e.CellBounds.Bottom - 1 , e.CellBounds.Right - 1 ,
                            e.CellBounds.Bottom - 1);
                        e.Graphics.DrawLine(gridLinePen , e.CellBounds.Right - 1 ,
                            e.CellBounds.Top , e.CellBounds.Right - 1 ,
                            e.CellBounds.Bottom);

                        // Draw the inset highlight box.
                        //e.Graphics.DrawRectangle(Pens.Black , newRect);

                        // Draw the text content of the cell, ignoring alignment.
                        if (e.Value != null)
                        {
                            e.Graphics.DrawString(e.Value.ToString() , e.CellStyle.Font ,
                                color , e.CellBounds.X + 2 ,
                                e.CellBounds.Y + 2 , StringFormat.GenericDefault);
                        }
                        e.Handled = true;
                    }
                }




            }
        }



        //private void dgv_SearchResult_ColumnHeaderMouseClick(object sender , DataGridViewCellMouseEventArgs e)
        //{
        //    dynamic dy = (dynamic)dgv_SearchResult.DataSource;

        //    if (e.ColumnIndex == 13)
        //    {
        //        //dy = from i in (IEnumerable<dynamic>)dy
        //        //     orderby i.explicit_01 descending
        //        //     select i;
        //    }

        //    dgv_SearchResult.DataSource = dy;
        //    //dgv_SearchResult.Sort(dgv_SearchResult.Columns[1] , System.ComponentModel.ListSortDirection.Descending);
        //    //dgv_SearchResult.Sort(dgv_SearchResult.Columns[12] , ListSortDirection.Ascending);
        //}

    }
}




// TODO : Adjust Image Position
// 圖片改成點到哪列就會換圖片
// 下一次做出下一頁按鈕 (Branch)
// TODO : Add 眾數 Lable 